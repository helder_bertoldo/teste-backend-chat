## Getting Started

### Installation Steps

Clone the repository

```sh
$ git clone https://helder_bertoldo@bitbucket.org/helder_bertoldo/teste-backend-chat.git
$ cd teste-backend-chat
```

Install the dependencies

```sh
$ npm install
# or
$ yarn install
```

Setup environment variables (modify/add more variables if needed)

```sh
$ cp .env.example .env
```

### Commands

> [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) are required to this way

Start application

```sh
# locally:
$ docker-compose up
```

Install migrations

```
$ docker exec -it chat_ioasys-app npm run sequelize:migrate
```

Revert migrations

```
$ docker exec -it chat_ioasys-app npm run sequelize:undo
```

Testing

```sh
# Run tests
$ docker exec -it chat_ioasys-app npm test
```

## Documentation

You might want to check the API docs as well!

- Collection on [Postman](https://documenter.getpostman.com/view/2660803/SzmZcfU6);
- Swagger: Just start the application at your desired `host:port` and use the route: `/api/v1/documentation` to open the swagger docs;

## License

[MIT](https://opensource.org/licenses/MIT)